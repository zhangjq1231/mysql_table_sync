#!/usr/bin/env bash
#检查python3是否安装
_msg() {
    color_off='\033[0m' # Text Reset
    case "${1:-none}" in
    red | error | err) color_on='\033[0;31m' ;;        # Red
    green | info) color_on='\033[0;32m' ;;             # Green
    yellow | warning | warn) color_on='\033[0;33m' ;;  # Yellow
    blue) color_on='\033[0;34m' ;;                     # Blue
    purple | question | ques) color_on='\033[0;35m' ;; # Purple
    cyan) color_on='\033[0;36m' ;;                     # Cyan
    time)
        color_on="[+] $(date +%Y%m%d-%T-%u), "
        color_off=''
        ;;
    stepend)
        color_on="[+] $(date +%Y%m%d-%T-%u), "
        color_off=' ... '
        ;;
    step)
        STEP=$((${STEP:-0} + 1))
        color_on="\033[0;35m[${STEP}] $(date +%Y%m%d-%T-%u), \033[0m"
        color_off=' ... '
        ;;
    *)
        color_on=''
        color_off=''
        need_shift=0
        ;;
    esac
    [ "${need_shift:=1}" -eq 1 ] && shift
    echo -e "${color_on}$*${color_off}"
}
_command_exists() {
    command -v "$@"
}
_check_sudo() {
    [[ "$check_sudo_flag" -eq 1 ]] && return 0
    if [ "$USER" != "root" ]; then
        if sudo -l -U "$USER" | grep -q "ALL"; then
            pre_sudo="sudo"
        else
            echo "User $USER has no permission to execute this script!"
            echo "Please run visudo with root, and set sudo to $USER"
            return 1
        fi
    fi
    if _command_exists apt; then
        cmd="$pre_sudo apt"
    elif _command_exists yum; then
        cmd="$pre_sudo yum"
    elif _command_exists dnf; then
        cmd="$pre_sudo dnf"
    else
        _msg time "not found apt/yum/dnf, exit 1"
        return 1
    fi
    check_sudo_flag=1
}
_check_dependence() {
    _command_exists curl || {
        $cmd update
        $cmd install -y curl
    }
    _command_exists git || {
        $cmd install -y git zsh
    }
    _command_exists wget || {
        $cmd install -y wget zsh
    }
    if [[ "$set_sysctl" -eq 1 ]]; then
        echo 'vm.overcommit_memory = 1' | $pre_sudo tee -a /etc/sysctl.conf
    fi
    if grep -q '^ID=.*alinux.*' /etc/os-release; then
        $pre_sudo sed -i -e '/^ID=/s/alinux/centos/' /etc/os-release
        aliyun_os=1
    fi
}
_install_dependence() {
    _check_dependence
    if _command_exists yum ;then
        $cmd install -y gcc zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel libffi-devel
    fi
    if _command_exists apt ;then 
        $cmd install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev
    fi
}
_check_user(){
    if [[ "$USER" == "root" ]];then
    install_path=/$USER
    else
    install_path=/home/$USER
    fi
}
_check_install_registry() {
    _check_user
    url_git=https://gitee.com/zhangjq1231/mysql_table_sync.git
    if [[ -f "$install_path"/mysql_table_sync/mysql_table_sync.py && -f "$install_path"/mysql_table_sync/install_python3/install_python3.sh ]]; then
        return 0
    else
    ## clone install_k8s or git pull
        $pre_sudo git clone -b master --depth 1 "$url_git" "$install_path"/mysql_table_sync
    fi
}
_install_python3(){
    if _command_exists /usr/local/python3/bin/python3;then
        echo `/usr/local/python3/bin/python3 --version`
    else
        cd "$install_path"/mysql_table_sync/install_python3/python3/
        $pre_sudo ./configure --prefix=/usr/local/python3 --with-ssl
        $pre_sudo make && $pre_sudo make install
        $pre_sudo /usr/local/python3/bin/pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
        $pre_sudo /usr/local/python3/bin/pip3 install --upgrade pip
        $pre_sudo /usr/local/python3/bin/pip3 install pymysql
        cd -
    fi
}
main(){
    _check_sudo
    _install_dependence
    _check_install_registry
    _install_python3
}
main "$@"
