import pymysql
import os
import sys
#初始的数据表列表
tables1=[]
#需同步的数据表列表
tables2=[]
#待同步的数据表列表
compare_list=[]

#测试环境（需要更新正式环境的数据库表结构）的数据库连接配置
db_connection_default = pymysql.connect(
        host="192.168.1.228",
        port=3306,
        user="root",
        password="aKlL2jADZK",
        database="defaultdb",
        charset="utf8"
    )
#正式环境的数据库连接配置
db_connection_test = pymysql.connect(
        host="192.168.1.228",
        port=3306,
        user="root",
        password="aKlL2jADZK",
        database="test",
        charset="utf8"
    )

def show_tables(db_connection,tables):
    cursor = db_connection.cursor()
    cursor.execute("show tables")
    data = cursor.fetchall()
    for i in data:
        for j in i:
            tables.append(j)
    cursor.close()
    db_connection.close()

#对比测试环境的数据库表和正式环境的数据库表，将正式环境中新增的数据库表提取出来
def compare(tables1,tables2):
    print("正在比对数据库.........")
    for i in tables2:
        if i not in tables1:
            compare_list.append(i)
            print(format("已比对到一个表,表名为:  %s")%i)
    print("\n共",len(compare_list),"个数据表\n")
    if len(compare_list)==0 :
        print("无需要更新的数据表.........")
        sys.exit(1)
#导出待同步的数据表结构

def dump_table_structure():
    show_tables(db_connection_default,tables1)
    show_tables(db_connection_test,tables2)
    compare(tables1,tables2)
    if os.path.isfile('compare.sql'):
        os.remove('compare.sql')
    for i in compare_list:
        db_connection_test.ping(reconnect=True)
        cursor = db_connection_test.cursor()
        cursor.execute(f"show create table {i}")
        result = cursor.fetchone()
        with open(f"compare.sql", "a+") as f:
             result_top=result[0]
             f.write(f"DROP TABLE IF EXISTS `{result_top}`;\n")
             f.write(f"{result[-1]} ROW_FORMAT = DYNAMIC;\n")
        cursor.close()
        db_connection_test.close()

dump_table_structure()

def update_table():
    db_connection_default.ping(reconnect=True)
    cursor=db_connection_default.cursor()
    try:
        results,results_list="",[]
        with open("compare.sql",mode="r+") as r:

            for sql in r.readlines():
                if not sql.startswith("--") and not sql.endswith("--"):
                    if not sql.startswith("--"):
                        results = results + sql

            #以;为分割，分割后会去除;
            for i in results.split(";"):
                if i.startswith("/*"):
                    results_list.append(i.split("*/")[1]+";")
                else:
                    results_list.append(i+";")
            print("开始更新数据表！")
            for x in results_list[:-1]:
                if x != ";":
                    cursor.execute(x)
                    db_connection_default.commit()
            print("更新表结构成功！")
            cursor.close()
            db_connection_default.close()
    except Exception as Exit:
        db_connection_default.rollback()
        cursor.close()
        db_connection_default.close()
        print("更新表结构失败：",Exit)

update_table()

